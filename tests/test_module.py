# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class LocationTestCase(ModuleTestCase):
    """Test module"""
    module = 'stock_location_notebook'

    def setUp(self):
        super(LocationTestCase, self).setUp()


del ModuleTestCase
